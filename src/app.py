from flask import Flask, render_template, request, session
import spotipy
from spotify_handler import SpotifyHandler
import uuid
from datetime import datetime
import edlib
import math
import sys
from countries_gps import countries
from geopy.distance import great_circle

app = Flask('spotify-jingle-manager')


MAX_INT = 9999999

@app.template_filter('track_duration')
def get_track_duration(duration_ms):
    seconds = int((duration_ms / 1000) % 60)
    minutes = int((duration_ms / (1000 * 60)) % 60)
    return "%d:%02d" % (minutes, seconds)


@app.template_filter('artist_name')
def get_artist_name(artists):
    return ', '.join([artist['name'] for artist in artists])

@app.template_filter('round')
def get_rounded(number):
    return round(number, 4)

@app.route('/', methods=['GET'])
def index():
    state = session.get('user_id', str(uuid.uuid4()))
    session['user_id'] = state

    token, auth_url = SpotifyHandler.login(
        state=request.args.get('state', default=state, type=str),
        code=request.args.get('code', type=str)
    )

    if not token:
        return render_template(
            'auth_required.html', auth_url=auth_url, user=None
        )

    spotify = spotipy.Spotify(auth=token)
    user = spotify.me()

    return render_template(
        'index.html', auth_url=None, user=user
    )


@app.route('/', methods=['POST'])
def search():
    token, auth_url = SpotifyHandler.login(state=session['user_id'])
    spotify = spotipy.Spotify(auth=token)

    # TODO: change from 10 to 50
    data = spotify.search(q='track:'+request.form['track-name'], type="track", limit=10)
    print(request.form, file=sys.stderr)
    original_tracks = data['tracks']['items']   
    
    # add info about album
    original_tracks = load_album_data(spotify, original_tracks)
    print(("len(original_items)=%s" % str(len(original_tracks))), file=sys.stderr)
    evaluated_tracks = evaluate(list(original_tracks), request.form)
    print("items evaluated", file=sys.stderr)
    
    sorted_tracks = sorted(evaluated_tracks, key=lambda track: track['score'])
    print("items sorted", file=sys.stderr)
    
    return render_template('partials/results.html', original_results=original_tracks, sorted_results=sorted_tracks)


def min_distance(selected_country, available_markets):
    """
    Search the nearest country from available markets
    and return distance. It is zero mostly.
    """
    min_d = MAX_INT
    if not selected_country or not available_markets:
        return min_d
    selected_country_gps = get_gps(selected_country)
    for market in available_markets:
        market_country_gps = get_gps(market)
        min_d = min(min_d, get_distance(selected_country_gps, market_country_gps))
    return min_d


def get_gps(country):
    country = country.upper()
    return countries[country]['latitude'], countries[country]['longitude']


def load_album_data(spotipy, results):
    ids = [track['album']['id'] for track in results]

    albums = spotipy.albums(ids)

    indexed_albums = dict()
    for album in albums['albums']:
        indexed_albums[album['id']] = album

    for item in results:
        item['album'] = indexed_albums[item['album']['id']]

    return results


def add_score(results, form_data):
    sorted_results = list(results)

    for item in sorted_results:
        item['scores'] = evaluate(item, form_data)
        print(item['scores'], file=sys.stderr)


def get_distance(a, b):
    lat1, lon1 = a
    lat2, lon2 = b

    dLat = math.radians(lat2 - lat1)
    dLon = math.radians(lon2 - lon1)

    a = (math.sin(dLat / 2) * math.sin(dLat / 2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dLon / 2) * math.sin(dLon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = 6378137 * c

    return d

#
# def get_distance(a, b):
#     return great_circle(a, b).miles


def evaluate(tracks: list, form_data):
    print("evaluate: is going to evaluate", file=sys.stderr)
    for track in tracks:
        track['evaluations'] = add_evaluations(track, form_data)
    print("evaluate: is going to normalize", file=sys.stderr)
    for track in tracks:
        track['evaluations'] = normalize(track['evaluations'], tracks)
    print("evaluate: is going to add weight", file=sys.stderr)
    for track in tracks:
        track['score'] = weight(track['evaluations'], form_data)
    return tracks


def add_evaluations(track, form_data):
    return {
        'popularity': popularity(track, form_data),
        'duration': duration(track, form_data),
        'artist_name': artist_name(track, form_data),
        'release_date': release_date(track, form_data),
        'distance': min_distance(form_data.get('region'), track.get('available_markets'))
    }


def weight(scores, form_data):
    score = 0

    for key, value in scores.items():
        if key + "_weight" not in form_data:
            continue

        score += value * (int(form_data[key + "_weight"]) / 100)

    return score


def normalize(evaluations, tracks):
    normalized = {}

    for attribute, evaluation in evaluations.items():
        max_evaluate = max([track['evaluations'][attribute] for track in tracks])

        if max_evaluate == 0:
            normalized[attribute] = 0
        else:
            normalized[attribute] = evaluation/max_evaluate

    print(("normalized: %s -> %s" % (str(evaluations), str(normalized))), file=sys.stderr)
    return normalized


def popularity(track, form_data):
    if not 'popularity_active' in form_data:
        return 0

    return abs(track['popularity'] - int(form_data['popularity']))


def duration(track, form_data):
    if not 'duration_active' in form_data:
        return 0

    return abs(track['duration_ms'] - int(form_data['duration']) * 1000) / 1000


def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            ins = previous_row[j + 1] + 1
            dels = current_row[j] + 1
            subs = previous_row[j] + (c1 != c2)
            current_row.append(min(ins, dels, subs))
        previous_row = current_row

    return previous_row[-1]


def artist_name(song, form_data):
    if not 'artist_name' in form_data:
        return 0

    names = get_artist_name(song['artists'])
    distance = levenshtein(names, form_data['artist_name'])
    return distance


def release_date(song, form_data):
    if not 'release_date' in form_data:
        return 0

    d1 = datetime.strptime(form_data['release_date'], "%d/%m/%Y")
    try:
        d2 = datetime.strptime(song['album']['release_date'], "%Y-%m-%d")
    except ValueError:
        d2 = datetime.strptime(song['album']['release_date'], "%Y")

    return abs((d1 - d2).days)

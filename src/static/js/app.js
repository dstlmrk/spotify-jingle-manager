$('.active-switcher').change(function () {
    var $e = $(this);
    var controls = $e.data('controls');
    $(controls).find('input').attr('disabled', !$e.is(':checked'));

    if ($e.is(':checked')) {
        $(controls).removeClass('disabled');
    } else {
        $(controls).addClass('disabled');
    }

    $(window).resize();

}).change();

$('#duration').change(function () {
    var val = $(this).val();
    var minutes = Math.floor(val / 60);
    $('.duration-display').text(minutes+" min "+(val - minutes*60)+" s");
}).change();


$('#similarity').change(function () {
    var val = $(this).val();
    $('.similarity-display').text(val+" %");
}).change();

var map;
$(document).ready(function () {
    $('.datepicker').datepicker();
    map = $('#vmap').vectorMap({
        map: 'world_en',
        multiSelectRegion: false,
        enableZoom: true
    });

    // map zooming
    // work only in Firefox
    $('#vmap').bind('DOMMouseScroll', function(e){
        if(e.originalEvent.detail > 0) {
            map.zoomOut();
        } else {
            map.zoomIn();
        }
        return false;
    });

    // work in Chrome...
    $('#vmap').bind('mousewheel', function(e){
     if(e.originalEvent.wheelDelta < 0) {
         map.zoomOut();
        } else {
            map.zoomIn();
        }
     return false;
    });
});

$('form').on('submit', function (e) {
    e.preventDefault();

    var action = $(this).data('action');
    var method = $(this).attr('method');

    var data = $(this).serialize();

    for (i in map.selectedRegions) {
        var region = map.selectedRegions[i];
        data += "&region="+region;
    }

    $.ajax({
        method: method,
        url: action,
        data: data,
        success: function(data) {
            $('#results').attr('hidden', false);
            $('#results').html(data)
        }
    });

    return false;
});
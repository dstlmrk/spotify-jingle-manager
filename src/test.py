import sys
import spotipy
import spotipy.util as util

CLIENT_ID = '5b9207e939fd4478bddb6205d9acb81a'
CLIENT_SECRET = '4e31b9a5c65345f1ba9ed6e6ffec9887'

# token = util.oauth2.SpotifyClientCredentials(client_id=CLIENT_ID, client_secret=CLIENT_SECRET)

scope = 'playlist-modify-public'
token = util.prompt_for_user_token('michalkvacek', scope, client_id=CLIENT_ID, client_secret=CLIENT_SECRET, redirect_uri='http://localhost')

#cache_token = token.get_access_token()
spotify = spotipy.Spotify(auth=token)
name = 'Radiohead'


def printResult(items):
    print('---------------------------------')
    print('Nalezeno ', len(items), " zaznamu")
    for track in items:
        print(', '.join([artist['name'] for artist in track['artists']]), ': ', track['name'], track['popularity'])


limit = 50
for i in range(0, 5):
    offset = i * limit
    results = spotify.search(q='artist:' + name, offset=offset, limit=limit)
    items = results['tracks']['items']

    printResult(items)
from spotipy.oauth2 import SpotifyOAuth
import os


class SpotifyHandler(object):

    @staticmethod
    def login(state, code=None):

        try:
            client_id = os.environ['SPOTIFY_CLIENT_ID']
            client_secret = os.environ['SPOTIFY_CLIENT_SECRET']
        except KeyError:
            print('''
                You need to set your Spotify API credentials. You can do this by
                setting environment variables like so:

                export SPOTIFY_CLIENT_ID='your-spotify-client-id'
                export SPOTIFY_CLIENT_SECRET='your-spotify-client-secret'

                Get your credentials at
                    https://developer.spotify.com/my-applications
            ''')

        sp_oauth = SpotifyOAuth(
            client_id=client_id,
            client_secret=client_secret,
            redirect_uri='http://localhost/',
            scope='user-modify-playback-state',
            cache_path=".cache/" + state,
            state=state
        )

        # try to get a valid token for this user, from the cache,
        # if not in the cache, the create a new (this will send
        # the user to a web page where they can authorize this app)

        token_info = sp_oauth.get_cached_token()

        if not token_info:
            if not code:
                auth_url = sp_oauth.get_authorize_url()
                return None, auth_url
            else:
                token_info = sp_oauth.get_access_token(code)

        if token_info:
            return token_info['access_token'], None
        else:
            return None, None

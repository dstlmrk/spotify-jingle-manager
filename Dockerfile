FROM python:3.6.3-alpine

ENV TZ Europe/Prague

RUN apk add --no-cache tzdata libstdc++
# mariadb-client-libs snappy build-base
WORKDIR /app/src
ADD requirements.txt ../
# ENV INSTALL_PACKAGES build-base mysql-dev snappy-dev
ENV INSTALL_PACKAGES build-base linux-headers bash git openssh
RUN apk add --no-cache $INSTALL_PACKAGES && \
    pip install --no-cache-dir -r ../requirements.txt && \
    apk del $INSTALL_PACKAGES

ADD ./src/ ./
ADD ./conf/uwsgi.ini ../
CMD ["uwsgi", "--ini", "../uwsgi.ini"]

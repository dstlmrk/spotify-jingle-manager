TODO: Generuje se pomoci: pandoc README.md -o neco.pdf

# Spotify metadata reranking

Autoři: **Marek Dostál** (dostam12) a **Michal Kváček** (kvacemic)

## Popis projektu

Projekt implementující metadata reranking nad daty ze Spotify. Pro funkčnost aplikace je zapotřebí mít aktivní účet na službě Spotify. Vstupem je textový řetězec podle kterého se hledají písničky. Dalšími parametry lze docílit seřazení získaných skladeb ze Spotify API:

* oblíbenost
* jméno umělce
* délka skladby
* datum vydání alba
* dostupnost přehrání skladby v konkrétních zemích

Implementace je napsaná v jazyce Python a používá webový framework Flask a technologii Docker pro virtualizaci.

## Způsob řešení

Celkové skóre pro zobrazení seřazených dat se počítá jako suma všech atributů násobená jejich vahou. Atributy se ještě před sčítáním normalizují na hodnoty mezi 0 a 1, kde nula odpovídá totožným atributům a jednička maximálně vzdáleným. Normalizace probíhá tak, že hodnotu dělíme maximem daného atributu.

### Oblíbenost

Počítáme absolutní hodnotu rozdílu vstupu uživatele od popularity dané skladby. Hodnoty na vstupu se pohybují od 0 do 100.

### Jméno umělce

Pro výpočet atributu používáme editační vzdálenost (Lehvensteinovu).

### Délka skladby

Délka skladby je zpracovávána podobně jako výše uvedená oblíbenost. Jen s tím rozdílem, že tato hodnota nemá horní mez. Opět platí, že 0 znamená absolutní shodu a cokoliv většího značí větší rozdíl.

### Datum vydádní skladby (resp. alba)

Pro řazení podle data vydání používáme rozdíl ve dnech mezi datem obdrženým ze Spotify a vstupem od uživatele.

### Dostupnost v zemích

Pro výpočet vzdáleností mezi státy používáme GPS souřadnice získané z [Google API](https://developers.google.com/public-data/docs/canonical/countries_csv). Nad těmito daty počítáme vzdálenost podle Great circle distance algoritmu. 

Řazení podle tohoto parametru je poměrně obtížné prezentovat, neboť většina skladeb je dostupná na všech trzích, kde Spotify působí.

### Řazení

Samotné řazení probíhá pomocí funkce sorted v Pythonu, která data seřadí vzestupně podle skóre (skóre = vážená vzdálenost mezi objekty).

## Příklad výstupu

Na následujících dvou obrázcích si lze prohlédnout formulář pro vstup řazení písníček a výstup aplikace, kde nalevo je původní seznam seřazený díky Spotify API a napravo seřazený dle požadavků uživatele. Pravý sloupec je ještě doplněn hodnotami vzdáleností, podle kterých se celý seznam řadil.

![Formulář pro vstupní parametry](./params.png "Vstupní parametry")

![Výstup](./results.png "Výstup")

## Experimentální sekce

Spotfiy API je omezeno a vrací při hledání maximálně 50 písniček. Na takto malém vzorku nelze dělat závěry pro rychlost výpočtu, protože v měření na 10 i 50 písníčkách byla majoritní položkou síťová komunikace.

Přesnost výpočtů s parametry zadanými od uživateli jsme testovali na několika vzorových příkladech a porovnávali jsme výstup aplikace, kde byly všechny hodnoty (vzdálenosti) jednotlivých parametrů uvedeny.

## Diskuze

Jediný a zároveň poměrně velký nedotatek je v hledání skladeb podle dostupných trhů (zemí), ve kterých lze skladbu přehrát. Jak je již výše uvedeno, není snadné řazení tohoto parametru jednoduše prezentovat. Nejlepší by bylo najít úplně jiný parametr pro hledání vzdálenosti mezi pouze dvěma body. Spotify API ale neposkytuje žádné jiné geo údaje o skladbách.

## Závěr

Pro seznámení s metadata rerankingem je tato práce naprosto dostačující. V projektu jsme si vyzkoušeli práci s několika algoritmy a normalizací jednotlivých parametrů.
